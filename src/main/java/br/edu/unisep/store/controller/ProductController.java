package br.edu.unisep.store.controller;

import br.edu.unisep.store.domain.dto.ProductDto;
import br.edu.unisep.store.domain.dto.RegisterProductDto;
import br.edu.unisep.store.domain.usecase.FindAllProductsUseCase;
import br.edu.unisep.store.domain.usecase.FindCustomerByIdUseCase;
import br.edu.unisep.store.domain.usecase.FindProductByIdUseCase;
import br.edu.unisep.store.domain.usecase.RegisterProductsUseCase;
import org.hibernate.tool.schema.internal.exec.ScriptTargetOutputToStdout;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    @GetMapping
    public ResponseEntity<List<ProductDto>> findAll() {
        var useCase = new FindAllProductsUseCase();
        var result = useCase.execute();

        if (result.isEmpty()) {
            return  ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDto> findById(@PathVariable("id") Integer id) {
        var useCase = new FindProductByIdUseCase();
        var product = useCase.execute(id);

        if (product == null) {
            return  ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(product);
    }

    @PostMapping
    public void save(@RequestBody RegisterProductDto product) {
        var useCase = new RegisterProductsUseCase();
        useCase.execute(product);
    }
}
