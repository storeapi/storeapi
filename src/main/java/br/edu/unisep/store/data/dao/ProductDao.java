package br.edu.unisep.store.data.dao;

import br.edu.unisep.store.data.entity.Products;
import br.edu.unisep.store.data.hibernate.HibernateSessionFactory;

import java.util.List;

public class ProductDao {

    public List<Products> findAll() {
        var session = HibernateSessionFactory.getSession();

        var query = session.createQuery("from Products", Products.class);
        var result = query.list();

        session.close();

        return result;
    }

    public Products findById(Integer id) {
        var session = HibernateSessionFactory.getSession();

        var query = session.createQuery("from Products where id = :pId", Products.class);
        query.setParameter("pId", id);
        var result = query.uniqueResult();

        session.close();

        return result;
    }

    public void save(Products products) {
        var session = HibernateSessionFactory.getSession();
        var transaction = session.beginTransaction();

        try {
            session.save(products);
            transaction.commit();
        } catch(Exception error) {
            error.printStackTrace();
            transaction.rollback();
        }

        session.close();
    }

    public void delete(Products products) {
        var session = HibernateSessionFactory.getSession();
        var transaction = session.beginTransaction();

        try {
            session.delete(products);
            transaction.commit();
        } catch(Exception error) {
            error.printStackTrace();
            transaction.rollback();
        }

        session.close();
    }

}
