package br.edu.unisep.store.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "products")
public class Products {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private Integer id;

    @Column(name = "name")
    private String nome;

    @Column(name = "description")
    private String descricao;

    @Column(name = "price")
    private Float valor;

    @Column(name = "brand")
    private String brand;

    @Column(name = "status")
    private  String status;
}
