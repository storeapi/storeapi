package br.edu.unisep.store.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ProductDto {

    private final Integer id;
    private final String nome;
    private final String descricao;
    private final Float valor;
    private final String brand;
    private final String status;
}
