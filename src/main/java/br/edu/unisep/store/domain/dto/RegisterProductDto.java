package br.edu.unisep.store.domain.dto;
import lombok.Data;

@Data
public class RegisterProductDto {

    private Integer id;
    private String nome;
    private String descricao;
    private Float valor;
    private String brand;
    private String status;

}
