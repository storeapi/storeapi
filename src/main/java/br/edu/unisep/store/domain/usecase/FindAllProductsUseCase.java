package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.dao.ProductDao;
import br.edu.unisep.store.domain.dto.ProductDto;

import java.util.List;
import java.util.stream.Collectors;

public class FindAllProductsUseCase {

    public List<ProductDto> execute() {
        var dao = new ProductDao();
        var product = dao.findAll();

        return product.stream().map(products -> {
            return new ProductDto(
                    products.getId(),
                    products.getNome(),
                    products.getDescricao(),
                    products.getValor(),
                    products.getStatus(),
                    products.getBrand()
            );
        }).collect(Collectors.toList());
    }
}
