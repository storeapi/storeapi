package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.dao.ProductDao;
import br.edu.unisep.store.domain.dto.ProductDto;

public class FindProductByIdUseCase {

    public ProductDto execute(Integer id) {

        if (id == null || id <=0 ) {
            throw new IllegalArgumentException("Id do produto deve ser informado.");
        }
        var dao = new ProductDao();
        var products = dao.findById(id);

        if (products != null) {
            return new ProductDto(
                    products.getId(),
                    products.getNome(),
                    products.getDescricao(),
                    products.getValor(),
                    products.getBrand(),
                    products.getStatus()
            );
        }
        return null;
    }
}
