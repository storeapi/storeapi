package br.edu.unisep.store.domain.usecase;

import br.edu.unisep.store.data.entity.Products;
import br.edu.unisep.store.domain.dto.RegisterProductDto;

public class RegisterProductsUseCase {

    public void execute(RegisterProductDto registerProduct) {
        var products = new Products();
        products.setNome(registerProduct.getNome());
        products.setDescricao(registerProduct.getDescricao());
        products.setBrand(registerProduct.getBrand());
        products.setStatus(registerProduct.getStatus());
        products.setValor(registerProduct.getValor());
    }

}
